import { NgModule } from '@angular/core';

import { DonutChartModule } from './components/index';


@NgModule({
    imports: [

    ],
    declarations: [

    ],
    exports: [
        DonutChartModule
    ]
})
export class NGenChartsModule { }
