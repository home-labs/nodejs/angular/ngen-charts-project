export interface IStrokeSettings {

    width: string;

    bindOn: Array<string>;

}
