export interface ISector {

    ngClass: object;

    length: number;

    offset: number;

}
